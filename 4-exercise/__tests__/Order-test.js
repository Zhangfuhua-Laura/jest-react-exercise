import React from 'react';

import '@testing-library/jest-dom/extend-expect';
import '@testing-library/react/cleanup-after-each';

import { render, fireEvent } from '@testing-library/react';

import axios from 'axios';
import 'babel-polyfill';

import Order from '../Order';

jest.mock('axios'); // Mock axios模块

test('Order组件显示异步调用订单数据', async () => {
  // <--start
  // TODO 4: 给出正确的测试
  // setup组件
  const { findByTestId, queryByLabelText, findByLabelText } = render(<Order />);
  // Mock数据请求
  const resp = { data: { status: '完成' } };
  axios.get.mockResolvedValue(resp);
  // 触发事件
  fireEvent.change(queryByLabelText('number-input'), { target: { value: '2' } });
  fireEvent.click(queryByLabelText('submit-button'));
  // 给出断言
  const number = await findByLabelText('number-input');
  expect(number).toHaveAttribute('value', '2');
  const actualStatus = await findByTestId('status');
  expect(actualStatus).toHaveTextContent('完成');
  // --end->
});
